class CustomFilter{
  constructor(token) {
    this.token = token;
    this.elements = '';
    this.queryText = '';
    this.paginate = 50;
    this.body = `
      title
      handle
      images(first: 1){
        edges {
          node {
            transformedSrc(maxWidth: 300)
          }
        }
      }
    `;
    this.collection = '';
    this.cursor = '';
  }
  
  query(){
    if(this.cursor){
      this.cursor = ` after: ${this.cursor}`
    }
    return `{
      products(first: ${this.paginate}, query:"${this.queryText}"${this.cursor}) {
        edges {
          cursor
          node {
            ${this.body}
          }
        }
        pageInfo {
          hasNextPage
        }
      }
    }`
  }

  buildQuery(){
    const queryArray = [];
    if(this.collection){
      queryArray.push(`collection:${this.collection}`)
    }
    const queryItems = this.elements.reduce((acc, item) => {
      const dataQuery = item.getAttribute('data-query');
      const dataComp = item.getAttribute('data-comp');
      const eleValue = item.getAttribute('data-value') ? item.getAttribute('data-value') : item.value;
      
      if(dataQuery && dataComp && eleValue){
        acc.push(dataQuery + dataComp + eleValue);
      }
      return acc;
    },[])
    return queryArray.concat(queryItems);
  }

  setParams({collection, operator, paginate, body, cursor, elements}){
    this.elements = Object.values(document.querySelectorAll(elements));
    this.paginate = paginate ? paginate : this.paginate;
    this.body = body ? body : this.body;
    this.collection = collection ? collection : this.collection;
    this.cursor = cursor ? cursor : this.cursor;
    this.queryText = this.buildQuery().join(` ${operator.toUpperCase()} `);
  }

  request(){
    this.setParams(arguments[0]);

    return fetch('/api/2020-01/graphql.json', {
      method: 'POST',
      headers: {
          'X-Shopify-Storefront-Access-Token': this.token,
          'Content-Type': 'application/graphql',
      },
      body: this.query()
    });
  }
}

const jsFilter = new CustomFilter('36f110b7c9c533acfcb1893ad03764bb');
document.querySelector('form').addEventListener('submit', async function(e){
  e.preventDefault();
  const requestObject = {
    collection: 'all', 
    operator: "AND", 
    items: 20, 
    elements: '.js-filter:checked,[name="price-min"],[name="price-max"]'
  };
  const response = await jsFilter.request(requestObject).then(res => res.json()).then(r => r.data)
  console.log(response);
})